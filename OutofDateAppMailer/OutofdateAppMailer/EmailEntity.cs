﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutofdateAppMailer
{
    public class EmailEntity
    {
        public string id { get; set; }
        public string email { get; set; }
    }
}
