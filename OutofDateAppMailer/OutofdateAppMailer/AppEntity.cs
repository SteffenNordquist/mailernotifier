using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutofdateAppMailer
{
    [BsonIgnoreExtraElements]
    public class AppEntity
    {
        public ObjectId id { get; set; }
        public string AppName { get; set; }
        public DateTime RunningTime { get; set; }
        public string ExecutionTime { get; set; }
        public bool RunnedSuccesful { get; set; }
        public bool Success { get; set; }
        public string Status { get; set; }
        public string UpTimeRange { get; set; }
    }
}
