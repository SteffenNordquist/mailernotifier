using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace OutofdateAppMailer
{
    class Program
    {
        static void Main(string[] args)
        {
            MongoClient Client = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/ApplicationDatabase");
            MongoServer Server = Client.GetServer();
            MongoDatabase Database = Server.GetDatabase("ApplicationDatabase");
            MongoCollection<AppEntity> collection = Database.GetCollection<AppEntity>("applications");

            MongoClient Client2 = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/Devnetworks_Emails");
            MongoServer Server2 = Client2.GetServer();
            MongoDatabase Database2 = Server2.GetDatabase("Devnetworks_Emails");
            MongoCollection<EmailEntity> collection2 = Database2.GetCollection<EmailEntity>("emails");

            var outdatedApps = collection.FindAll().Where(x => !x.Success || !UpTimePassed(x) || x.RunningTime < DateTime.Now.AddHours(-24));

            string errorHeader = "The following tools are outdated and not running:";
            string successHeader = "Congratulations, your tools are all happy ";
            string apps = "";
            foreach (var outdatedApp in outdatedApps)
            {
                    apps = apps + "\n" + outdatedApp.AppName;
            }

            if (apps != "")
            {
                apps = errorHeader + "\n" + apps;
            }

            else
            {
                apps = successHeader;
            }

            var collectionEmails = collection2.FindAll();
            List<string> emailsList = new List<string>();

            foreach (var collectionEmail in collectionEmails)
            {
                emailsList.Add(collectionEmail.email);
            }

            foreach (string email in emailsList)
            {
                Mail(apps,email);
            }

            //Mail(apps);
        }

        static bool UpTimePassed(AppEntity app)
        {
            DateTime upTimeRangeComparer = DateTime.Now.AddHours(-24);

            if (app.UpTimeRange != null && app.UpTimeRange.ToLower().Contains("h"))
            {
                int upTimeRange = 0;
                Int32.TryParse(app.UpTimeRange.ToLower().Replace("h", ""), out upTimeRange);
                upTimeRangeComparer = DateTime.Now.AddHours(-upTimeRange);
            }
            else if (app.UpTimeRange != null && app.UpTimeRange.ToLower().Contains("m"))
            {
                int upTimeRange = 0;
                Int32.TryParse(app.UpTimeRange.ToLower().Replace("m", ""), out upTimeRange);
                upTimeRangeComparer = DateTime.Now.AddMinutes(-upTimeRange);
            }

            if (app.RunningTime < upTimeRangeComparer)
            {
                return false;
            }

            return true;
        }

        static void Mail(string body, string emailtosent)
        {
            MailAddress from = new MailAddress("devnetworks.vanessa@gmail.com");
            //MailAddress to = new MailAddress("vanessayting@gmail.com");
            MailAddress to = new MailAddress(emailtosent);
            string fromPassword = "devnetworksvan12345";
            string subject = "Following tools are outdated and not running:";
            string message = body;


            MailMessage mail = new MailMessage(from, to);
            mail.Subject = subject;
            mail.Body = message;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(from.ToString(), fromPassword),
                Timeout = 20000
            };


            try
            {
                smtp.Send(mail);
                Console.WriteLine("Mail sent to: " + to.ToString());

                //**Uncomment for final deployment
                //SetInvoiceAsEmailed(invoice.reference);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
